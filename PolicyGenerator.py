#EVERYTIME A PERSON GIVES A TEXTUAL INPUT THIS CODE SHOULD GET EXECUTED

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from random import shuffle
import sys
import requests
import json
import re
from wit import Wit
from flask import Flask,render_template,request,jsonify
app = Flask(__name__)
@app.route('/')
def index():
    return render_template('indexBot.html') 

access_token = 'VF5WFMTRWMIPVVSQO6L3VQ3QGOG7F3KI'
policy_value = ''

def first_entity_value(entities, entity):
    if entity not in entities:
        return None
    val = entities[entity][0]['value']
    if not val:
        return None
    return val['value'] if isinstance(val, dict) else val     

client = Wit(access_token=access_token)# this is to set up a connection with my wit bot
#I need to enter the user input here from the HTML file inside client.message()
@app.route('/bot', methods=["GET", "POST"])
def bot():
    if request.method == "POST":
        #handle_message(request.json)
        #print(request.json)
        #return jsonify({'response' :"Please enter your policy number"})
        userResponse=request.json
        print(userResponse)
        entities = userResponse['witResponse']['entities']
        intent = first_entity_value(entities,'intent')
        print("Intent: ",intent)
        #policy_num = first_entity_value(entities, 'policy_num')
        greetings = first_entity_value(entities, 'greetings')
        resp = userResponse['witResponse']['_text']
        policy_value=""
        agent_id=""
        s3_link=""
        if(validatePolicyNo(resp)==True):
            policy_value = resp
        elif(validateAgentID(resp)==True):
            agent_id = resp

        #Every return value below this function should be sent back to the html page
        if policy_value:
            return returnMessage("Please enter the agent ID")
        elif agent_id:
            print("inside IF block")
            agent_id=int(agent_id)
            print("Policy number: ", userResponse['policy_number'])
            headers = {'Content-Type': 'application/json'}
            body = {"login": "robo@godigit.com", "password": "Robo123"}
            response = requests.post('http://preprod-InsuranceActivity.godigit.com/InsuranceActivity/rest/auth/1',
                                     json=body, headers=headers)
            authToken = str(response.headers['Authorization'])
            headers1 = {'Authorization': authToken}
            pol = userResponse['policy_number']
            response1 = requests.get(
                'https://prod-DigitCareNew.godigit.com/DigitCareNew/getpageactivites?pageNo=1&PolicyNumber=' + str(
                    userResponse['policy_number']), headers=headers1)
            data = response1.json()
            print("Data ", data)
            agent_id_db =  data[0]['agentId']
            print("Agent ID ",agent_id_db)
            if agent_id == agent_id_db:
                s3_link = jsonify({'response':data[0]['documentDetails'][1]['s3Link']})
                print("s3 link: ", s3_link)
                return s3_link
            print("hahahaha")
            return returnMessage("Incorrect Agent ID or Policy Number!")
        elif intent == 'policy_pdf':
            return returnMessage("Please enter your policy number")
        elif intent == 'pi_link':
            return returnMessage("Please enter your policy number")
        elif greetings :
            return returnMessage("Hello, how may i help you?")
        else :
            return returnMessage("Try again! Enter a valid number!")

def returnMessage(message):
    return jsonify({'response' : message})

def validatePolicyNo(resp):
    x = re.match('[A-Z]{1}[0-9]{9}', resp)
    if x:
        return True
    else:
        return False

def validateAgentID(resp):
    x = re.match('[0-9]{7}', resp)
    if x:
        return True
    else:
        return False
if __name__ =="__main__":
    app.run(debug=True)



# def handle_message(response):
#     print(response)
#     entities = response['entities']
#     intent = first_entity_value(entities,'intent')
#     policy_num = first_entity_value(entities, 'policy_num')
#     greetings = first_entity_value(entities, 'greetings')
#     x=re.findall('[D0-90-90-90-90-90-90-90-90-9]',response['_text'])
#     delimiter=""
#     policy_value=delimiter.join(x)
#     print(policy_value)
#     #Every return value below this function should be sent back to the html page
#     if policy_value :
#         headers = {'Content-Type': 'application/json'}
#         body={"login": "robo@godigit.com","password": "Robo123"}
#         response=requests.post('http://preprod-InsuranceActivity.godigit.com/InsuranceActivity/rest/auth/1',json=body,headers=headers)
#         authToken=str(response.headers['Authorization'])
#         headers1 = {'Authorization':authToken}
#         response1=requests.get('https://prod-DigitCareNew.godigit.com/DigitCareNew/getpageactivites?pageNo=1&PolicyNumber='+str(policy_value),headers=headers1)
#         data=response1.json()
#         return data[0]['documentDetails'][1]['s3Link']
#     elif intent == 'policy_pdf':
#         return "Please enter your policy number"
#     elif greetings :
#        return jsonify({'respText' : 'Hello how may i help you?'})
#     else :
#         return 'try again'  